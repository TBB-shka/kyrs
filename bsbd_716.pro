#-------------------------------------------------
#
# Project created by QtCreator 2019-05-01T20:37:55
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bsbd_716
TEMPLATE = app


SOURCES += main.cpp\
    form_from_prep_1.cpp \
        mainwindow.cpp \
    logon.cpp \
    obor.cpp \
    reg.cpp \
    three.cpp

HEADERS  += mainwindow.h \
    form_from_prep_1.h \
    logon.h \
    obor.h \
    reg.h \
    three.h

FORMS    += mainwindow.ui \
    form_from_prep_1.ui \
    logon.ui \
    obor.ui \
    reg.ui \
    three.ui
