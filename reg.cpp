#include "reg.h"
#include "logon.h"
#include "mainwindow.h"
#include "ui_reg.h"
#include <random>
#include <ctime>
#include <QString>

Reg::Reg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Reg)
{
    ui->setupUi(this);
    ui->bd_connect->setEnabled(false);
    ui->serv_connect->setEnabled(false);
    ui->Ok_buttom->setVisible(false);
    ui->check_Box->setEnabled(false);
}

Reg::~Reg()
{
    delete ui;
}

void Reg::on_pushButton_clicked()
{
    QString prov;
    prov=ui->check_text->text();
    if (prov==rund)
    {
            ui->check_Box->setChecked(true);
            ui->Ok_buttom->setVisible(true);
    }
}

void Reg::on_pushButton_3_clicked()
{
    int rand = qrand()%10000+1000;
    rund=QString::number(rand);
    ui->pushButton_3->setToolTip(rund);
}

void Reg::on_Ok_buttom_clicked()
{
    dbr = QSqlDatabase::addDatabase("QODBC");
    dbr.setDatabaseName("DRIVER={SQL Server};SERVER="+ui->serv_connect->text()+";DATABASE="+ui->bd_connect->text()+";");
    dbr.setUserName("sa");
    dbr.setPassword("4865");

     if(dbr.open())
     {

         QSqlQuery* query = new QSqlQuery();
         query->prepare("Use [master];");
         if(!query->exec())
         {
             QMessageBox* mess = new QMessageBox();
             mess->setText("Запрос не правильный");
             mess->show();

         }
         else{
             QMessageBox* mess = new QMessageBox();
             mess->setText("Хорошо");
             mess->show();
         }
         query->prepare("CREATE LOGIN ["+ui->login_text->text()+"] WITH PASSWORD=N'"+ui->pass_text->text()+"', DEFAULT_DATABASE=[Kursovay], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF");
         if(!query->exec())
         {
             QMessageBox* mess = new QMessageBox();
             mess->setText("Запрос не правильный");
             mess->show();

         }
         else{
             QMessageBox* mess = new QMessageBox();
             mess->setText("Хорошо");
             mess->show();
         }
         query->prepare("Use [Kursovay];");
         if(!query->exec())
         {
             QMessageBox* mess = new QMessageBox();
             mess->setText("Запрос не правильный");
             mess->show();

         }
         else{
             QMessageBox* mess = new QMessageBox();
             mess->setText("Хорошо");
             mess->show();
         }
         query->prepare("CREATE USER ["+ui->login_text->text()+"] FOR LOGIN ["+ui->login_text->text()+"]");
         if(!query->exec())
         {
             QMessageBox* mess = new QMessageBox();
             mess->setText("Запрос не правильный");
             mess->show();

         }
         else{
             QMessageBox* mess = new QMessageBox();
             mess->setText("Хорошо");
             mess->show();
         }
         query->prepare("ALTER ROLE [Клиент] ADD MEMBER ["+ui->login_text->text()+"]");
         if(!query->exec())
         {
             QMessageBox* mess = new QMessageBox();
             mess->setText("Запрос не правильный");
             mess->show();

         }
         else{
             QMessageBox* mess = new QMessageBox();
             mess->setText("Хорошо");
             mess->show();
         }


         //query->prepare("Create user (name)without login VALUES (:name) ");
         // query->bindValue(":name",ui->login_text->text());


    //GRANT IMPERSONATE ON USER::[89237266309] TO [Klient]
}
}
