#ifndef OBOR_H
#define OBOR_H

#include <QWidget>
#include "logon.h"

namespace Ui {
class Obor;
}

class Obor : public QWidget
{
    Q_OBJECT

public:
    explicit Obor(QWidget *parent = nullptr);
    ~Obor();

private:
    Ui::Obor *ui;
    QSqlQueryModel *model;
};

#endif // OBOR_H
