#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pushButton_4->setVisible(false);
    ui->tableView_6->setVisible(false);
    ui->Role_2->setVisible(false);

    model = new QSqlQueryModel();
    model->setQuery("SELECT * FROM Клиент");

    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->show();

    /*if(model==NULL){
        QMessageBox* messi = new QMessageBox();
        messi->setText("Пусто, появись)))");
        messi->show();
        QSqlQuery* query = new QSqlQuery();
        query->prepare("select * from Клиент");

        QString temp;
        //temp = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),1)).toString();
        temp= ui->tableView->model()->data(ui->tableView->model()->index(0,1)).toString();
        qDebug()<<temp;
        if(temp==NULL){
                QMessageBox* messi = new QMessageBox();
                messi->setText("Пусто, появись)))");
                messi->show();*/
    }




MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_triggered()
{

}

void MainWindow::on_pushButton_clicked()
{
    /*
    if(ui->lineEdit_id->text() == "")
    {
        mess->setText("Вы не выбрали сотрудника");
        mess->show();
    }
    else
    {
        QSqlQuery* query = new QSqlQuery();
        query->prepare("update worker set firstname = :firstname, secondname = :secondname, thirdname = :thirdname, birthday = :birthday, phone_number = :phone_number, passport_number = :passport_number, modificateDate = (getdate()) where id_worker = :id_worker");
        query->bindValue(":firstname",ui->lineEdit_firstname->text());
        query->bindValue(":secondname",ui->lineEdit_secondname->text());
        query->bindValue(":thirdname",ui->lineEdit_thirdname->text());
        query->bindValue(":birthday", ui->dateEdit_birthday->date().toString("yyyy-MM-dd"));
        query->bindValue(":phone_number",ui->lineEdit_phonenumber->text());
        query->bindValue(":passport_number",ui->lineEdit_passportnumber->text());
        query->bindValue(":id_worker",ui->lineEdit_id->text().toInt());

        if(!query->exec())
        {
            mess->setText("Запрос составлен не верно");
            mess->show();
        }
        else
        {
            mess->setText("Запрос выполнен");
            mess->show();
            model->setQuery("SELECT * FROM worker");

            ui->lineEdit_id->setText("");
            ui->lineEdit_firstname->setText("");
            ui->lineEdit_passportnumber->setText("");
            ui->lineEdit_phonenumber->setText("");
            ui->lineEdit_secondname->setText("");
            ui->lineEdit_thirdname->setText("");

            ui->tableView->setModel(model);
            ui->tableView->resizeColumnsToContents();
        }
    }
    */
}

void MainWindow::on_pushButton_2_clicked()
{
    /*
    if(ui->lineEdit_id->text() == "")
    {
        mess->setText("Вы не выбрали сотрудника");
        mess->show();
    }
    else
    {
        QSqlQuery* query = new QSqlQuery();
        query->prepare("delete from worker where id_worker = :id_worker");

        query->bindValue(":id_worker",ui->lineEdit_id->text().toInt());

        if(!query->exec())
        {
            mess->setText("Запрос составлен не верно");
            mess->show();
        }
        else
        {
            mess->setText("Запрос выполнен");
            mess->show();
            model->setQuery("SELECT * FROM worker");

            ui->lineEdit_id->setText("");
            ui->lineEdit_firstname->setText("");
            ui->lineEdit_passportnumber->setText("");
            ui->lineEdit_phonenumber->setText("");
            ui->lineEdit_secondname->setText("");
            ui->lineEdit_thirdname->setText("");

            ui->tableView->setModel(model);
            ui->tableView->resizeColumnsToContents();
        }
    }
    */
}

void MainWindow::on_pushButton_3_clicked()
{
    QSqlQuery* query = new QSqlQuery();
    query->prepare("insert into Клиент (ФИО,Номер_телефона,Характеристика) VALUES (:fio,:number,:hara)");
    query->bindValue(":fio",ui->lineEdit_firstname->text());

    query->bindValue(":number",ui->lineEdit_phonenumber->text());
    query->bindValue(":hara",ui->textEdit->toPlainText());

    if(!query->exec())
    {
        mess->setText("Запрос составлен не верно");
        mess->show();
    }
    else
    {
        mess->setText("Запрос выполнен");
        mess->show();
        model->setQuery("SELECT * FROM клиент");


    }
}

void MainWindow::on_tableView_clicked(const QModelIndex &index)
{    
    QString temp;
    temp = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),1)).toString();

    QSqlQuery* query = new QSqlQuery();
    query->prepare("select * from Клиент where Предметная_область=:id_worker");
    query->bindValue(":id_worker",temp);
    if(query->exec())
    {
        query->next();
        ui->lineEdit_firstname->setText(query->value(0).toString());

        ui->lineEdit_phonenumber->setText(query->value(1).toString());
        ui->textEdit_2->setText(query->value(3).toString());

    }

    qDebug() << temp;
}

void MainWindow::on_pushButton_4_clicked()
{
     /*QString temp_2;
     //temp_2 = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),1)).toString();
    QSqlQuery* query = new QSqlQuery();
    query->prepare("WITH CTE_Roles (role_principal_id) AS ( SELECT role_principal_id FROM sys.database_role_members WHERE member_principal_id = USER_ID() UNION ALL SELECT drm.role_principal_id FROM sys.database_role_members drm INNER JOIN CTE_Roles CR ON drm.member_principal_id = CR.role_principal_id ) SELECT USER_NAME(role_principal_id) RoleName FROM CTE_Roles UNION ALL SELECT 'public' ORDER BY RoleName;");
    temp_2=query->value(0).toString();
    if(!query->exec())
    {
        mess->setText("Запрос составлен не верно");
        mess->show();
    }
    else
    {
        mess->setText("Запрос выполнен");
        mess->show();

    }
    qDebug()<<temp_2;

*/
}

void MainWindow::on_Role_2_clicked()
{/*
    QSqlQuery* query = new QSqlQuery();
    query->prepare("execute as user = '89237264980'");
    if(!query->exec())
    {
        mess->setText("Запрос составлен не верно");
        mess->show();
    }
    else
    {
        mess->setText("Запрос выполнен");
        mess->show();

    }
    model = new QSqlQueryModel();
    model->setQuery("SELECT * FROM Клиент");

    ui->tableView->setModel(model);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->show();
*/}

void MainWindow::on_tableView_6_activated(const QModelIndex &index)
{

}

void MainWindow::on_troika_clicked()
{
    Three* w = new Three();
    w->show();
    this->hide();

}
