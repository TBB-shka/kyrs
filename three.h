#ifndef THREE_H
#define THREE_H

#include <QDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QDebug>
#include <QDesktopServices>
#include "mainwindow.h"
#include "reg.h"
#include "logon.h"
#include <QWidget>

namespace Ui {
class Three;
}

class Three : public QWidget
{
    Q_OBJECT

public:
    explicit Three(QWidget *parent = nullptr);
    ~Three();

private slots:
    void on_tableView_clicked(const QModelIndex &index);

    void on_combo_pred_obl_currentTextChanged(const QString &arg1);

    void on_combo_pred_obl_currentIndexChanged(const QString &arg1);

    void on_insert_bottom_clicked();

    void on_checkBox_stateChanged(int arg1);

    void on_insert_bottom_graph_clicked();

    void on_graph_tabl_clicked(const QModelIndex &index);

    void on_label_10_linkActivated(const QString &link);

private:
    Ui::Three *ui;
    QSqlQueryModel *modeli;
    QSqlQueryModel *modelis;
    QSqlQueryModel *modelisi;
    QSqlQueryModel *model;
   QSqlQueryModel *modelus;
    QMessageBox* mess = new QMessageBox();
    QString prep;
};

#endif // THREE_H
