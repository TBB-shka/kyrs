#ifndef LOGON_H
#define LOGON_H

#include <QDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QDebug>

#include "mainwindow.h"
#include "reg.h"
#include "form_from_prep_1.h"

namespace Ui {
class logon;
}

class logon : public QDialog
{
    Q_OBJECT

public:
    explicit logon(QWidget *parent = 0);
    ~logon();    

private slots:
    void on_pushButton_clicked();

    void on_reg_bot_clicked();

    void on_rand_pass_bottom_clicked();

    void on_lineEdit_4_textChanged(const QString &arg1);

private:
    Ui::logon *ui;
    QSqlDatabase db;
    QSqlQueryModel *model_for_logon;
    QMessageBox* mess = new QMessageBox();


};

#endif // LOGON_H
