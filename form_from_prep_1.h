#ifndef FORM_FROM_PREP_1_H
#define FORM_FROM_PREP_1_H

#include <QWidget>
#include "logon.h"

namespace Ui {
class Form_from_prep_1;
}

class Form_from_prep_1 : public QWidget
{
    Q_OBJECT

public:
    explicit Form_from_prep_1(QWidget *parent = nullptr);
    ~Form_from_prep_1();

private slots:
    void on_commandLinkButton_clicked();

    void on_link_bottom_clicked();

    void on_predmet_table_clicked(const QModelIndex &index);

    void on_link_nedo_buttom_clicked();

    void on_param_prep_bottom_clicked();

    void on_tableView_aydit_clicked(const QModelIndex &index);

private:
    Ui::Form_from_prep_1 *ui;
    QSqlQueryModel *model;
    QSqlQueryModel *modelus;
    QSqlQueryModel *modelik;
    QSqlQueryModel *mod;
    QSqlQueryModel *mo;
     QString linki;
     QString cord;
     QString audit;
     QMessageBox* mess = new QMessageBox();
};

#endif // FORM_FROM_PREP_1_H
