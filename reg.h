#ifndef REG_H
#define REG_H

#include <QDialog>
#include "mainwindow.h"
#include <QMainWindow>
#include <QMessageBox>
#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QDebug>

namespace Ui {
class Reg;
}

class Reg : public QDialog
{
    Q_OBJECT

public:
    explicit Reg(QWidget *parent = nullptr);
    ~Reg();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_Ok_buttom_clicked();

private:
    Ui::Reg *ui;
    QSqlDatabase dbr;
    QString rund;
};

#endif // REG_H
